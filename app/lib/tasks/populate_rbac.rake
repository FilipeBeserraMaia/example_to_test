#frozen_string_literal: true

namespace :populate_rbac do
  desc 'populate users table'

  task :role => :environment do

    RbacRls::Role.where(email: "filipe.maia@gmail.com").first_or_create!(password: "123456")

  end

  task :rp => :environment do
    vendedores_role = RbacRls::Role.where(name: "Vendedores").first_or_create!(comments: "Vendedores apenas podem ver criar e atualizar os proprios produtos")
    gerentes_role = RbacRls::Role.where(name: "Gerentes").first_or_create!(comments: "Gerentes podem ver criar e atualizar produtos e apagar qualquer produto")
    repositores_role = RbacRls::Role.where(name: "Repositores").first_or_create!(comments: "Repositores podem apenas vizualizar produtos")

    #permissão de vendedor
    vendedores_permission = RbacRls::Permission.where(
      table_name: "products",
      read: true, #select
      write: true, #insert
      change: false, #update
      remove: false, #delete
      owner_read: false,
      owner_change: true,
      owner_remove: false).first_or_create!

    #permissão de Gerentes
    gerentes_permission = RbacRls::Permission.where(
      table_name: "products",
      read: true, #select
      write: true, #insert
      change: true, #update
      remove: true, #delete
      owner_read: false,
      owner_change: false,
      owner_remove: false).first_or_create!

    #permissão de Repositores
    repositores_permission = RbacRls::Permission.where(
      table_name: "products",
      read: true, #select
      write: false, #insert
      change: false, #update
      remove: false, #delete
      owner_read: false,
      owner_change: false,
      owner_remove: false).first_or_create!

    RbacRls::RolePermission.where(role_id: vendedores_role.id, permission_id: vendedores_permission.id).first_or_create!
    RbacRls::RolePermission.where(role_id: gerentes_role.id, permission_id: gerentes_permission.id).first_or_create!
    RbacRls::RolePermission.where(role_id: repositores_role.id, permission_id: repositores_permission.id).first_or_create!

  end

end
