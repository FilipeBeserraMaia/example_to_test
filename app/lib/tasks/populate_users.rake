#frozen_string_literal: true
# rake populate_users:make
namespace :populate_users do
  desc 'populate users table'

  task :make => :environment do

    ::User.where(email: "user.1@gmail.com").first_or_create!(password: "123456")
    ::User.where(email: "user.2@gmail.com").first_or_create!(password: "123456")
    ::User.where(email: "user.3@gmail.com").first_or_create!(password: "123456")
    ::User.where(email: "user.4@gmail.com").first_or_create!(password: "123456")
    ::User.where(email: "user.5@gmail.com").first_or_create!(password: "123456")

  end

end
