#frozen_string_literal: true

namespace :populate_products do

  desc 'populate users table'

  task :make => :environment do
    user_ids = User.all.map(&:id)
    p_values = (100...5000).to_a

    products = ["bolsa",
                "blusa",
                "calça",
                "tênis",
                "cinto",]

    products.each do |name|
      Product.where(name: name).first_or_create!(value: p_values.sample, description: "Product ", owner_id: user_ids.sample)
    end

  end

end

