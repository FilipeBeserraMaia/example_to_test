#frozen_string_literal: true

namespace :main do
  desc 'Run all Rakes to populate tables'

  task :start => :environment do

    rakes = ["populate_users:make", "populate_products:make", "populate_rbac:rp"]

    rakes.each do |rake_name|
      Rake::Task[rake_name].invoke
    end

  end

end
