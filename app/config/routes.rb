Rails.application.routes.draw do
  root 'home#index'
  devise_for :users

  resources :products
  resources :home, only: [:index]
#  mount RbacRls::Engine => :rbac_rls


end
